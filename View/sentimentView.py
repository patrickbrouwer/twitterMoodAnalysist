import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.dates as md
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
import matplotlib.pyplot as plt
import tkinter as Tk
from tkinter import messagebox
from Core.Database import Database
from numpy import arange

matplotlib.use('TkAgg')
plt.style.use('ggplot')

class SentimentGraph:
    figure = None
    axes = None

    timeStamps = []
    sentiment_score = []

    def __init__(self, create_figure=True):
        # get data from database.
        self.get_data()

        # create figure.
        if create_figure:
            self.figure = Figure()

        # draw graph in figure
        self.draw_graph(self.figure)

    def draw_graph(self, figure):

        neutral = 0
        negative = 0
        positive = 0

        for score in self.sentiment_score:
            if score == 0:
                neutral += 1
            elif score > 0:
                positive += 1
            elif score < 0:
                negative += 1

        #self.figure, axes = plt.subplots()
        #ax1, ax2 = axes.ravel()

        ax1 = self.figure.add_subplot(221)

        # The slices will be ordered and plotted counter-clockwise.
        labels = 'neutral', 'Negative', 'Positive'
        sizes = [neutral, positive, negative]
        colors = ['yellowgreen', 'lightcoral', 'lightskyblue']
        explode = (0, 0.1, 0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')

        ax1.pie(sizes, explode=explode, labels=labels, colors=colors,
                autopct='%1.1f%%', shadow=True, startangle=90,
                radius=0.25, center=(0, 0), frame=True)

        # Set aspect ratio to be equal so that pie is drawn as a circle.
        ax1.axis('equal')
        ax1.axis('off')

        ax2 = self.figure.add_subplot(122)

        ax2.plot_date(self.timeStamps, self.sentiment_score, alpha=0.5, picker=5)

        ax2.set_title("Sentiment score")
        ax2.set_ylabel("Sentiment score")
        xfmt = md.DateFormatter('%Y-%m-%d %H:%M')

        ax2.xaxis.set_minor_locator(HourLocator(arange(0, 25, 6)))
        ax2.xaxis.set_major_formatter(DateFormatter('%H:%M'))
        ax2.xaxis.set_major_formatter(xfmt)

        ax2.fmt_xdata = md.DateFormatter('%Y-%m-%d %H:%M')
        self.figure.autofmt_xdate()

    def get_data(self):
        db = Database()
        result = db.query(
            ''' select sentiment_score, posted_at / 1000 as timestamp from tweets ''')
        rows = result.fetchall()

        for row in rows:
            self.sentiment_score.append(row[0])
            # convert unix timestamp to matplotlib compatible
            date = matplotlib.dates.epoch2num(row[1])
            self.timeStamps.append(date)
        return True

    def onclick(self, event):
        ind = event.ind
        #get first index of the array
        ind = ind[0]

        #get x and y
        thisline = event.artist
        xdata = thisline.get_xdata() #timestamp
        ydata = thisline.get_ydata() #sentiment

        #cast to usefull stuff
        score = int(ydata[ind])
        timestamp = str(int(matplotlib.dates.num2epoch(xdata[ind])))
        timestamp = timestamp[:-1]

        #find it in the database
        db = Database()
        #print(" select * from tweets WHERE posted_at LIKE '"+ str(timestamp) +"%' AND sentiment_score = '"+ str(score) + "'" )
        result = db.query(" select * from tweets WHERE posted_at LIKE '"+ str(timestamp) +"%' AND sentiment_score = '"+ str(score) + "'")
        row = result.fetchone()

        #order data
        id = row[0]
        name = row[1]
        screenname = row[2]
        posted_at = row[3]
        tweet = row[4]
        location = row[7]
        score = row[10]

        #build the string
        strbuilder = []

        strbuilder.append("id : "+str(id))
        strbuilder.append("\nname : "+name.decode('ascii', 'ignore'))
        strbuilder.append("\nlocation : "+location)
        strbuilder.append("\nscore : "+str(score))
        strbuilder.append("\ntweet : "+tweet.decode('ascii', 'ignore'))
        #show me the good stuff
        messagebox.showinfo("info", ''.join(strbuilder))


if __name__ == "__main__":
    # change config db file location
    import config

    config.DB_FILE = "../tweets.db"

    # create window
    root = Tk.Tk()
    root.wm_title("time line")

    graph = SentimentGraph()


    def _quit():
        root.quit()  # stops mainloop
        root.destroy()  # this is necessary on Windows to prevent
        # Fatal Python Error: PyEval_RestoreThread: NULL tstate

    # a tk.DrawingArea
    canvas = FigureCanvasTkAgg(graph.figure, master=root)
    canvas.mpl_connect('pick_event', graph.onclick)
    canvas.show()
    canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

    toolbar = NavigationToolbar2TkAgg(canvas, root)
    toolbar.update()
    canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

    button = Tk.Button(master=root, text='Quit', command=_quit)
    button.pack(side=Tk.BOTTOM)

    Tk.mainloop()
    # If you put root.destroy() here, it will cause an error if
    # the window is closed with the window manager.
