from tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from Core.Database import Database
from View.timelineView import TimelineGraph
from View.sentimentView import SentimentGraph
import matplotlib
from View.tweetSearchStatusView import StatusView
import sys
import os
from tkinter import messagebox
matplotlib.use('TkAgg')


class Window(Frame):
    searchVar = ""
    db = Database()

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()

    def init_window(self):
        # changing the title of our master widget
        self.master.title("Twitter Analyse")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # menu bar
        self.create_menu_bar()

        # searchbar
        self.searchVar = StringVar()
        self.searchVar.set(self.db.getSearchQuery())

        Label(self, text="Tweet # search").grid(row=0, column=0)
        input_search = Entry(self, textvariable=self.searchVar, width=100).grid(row=0, column=1)
        Button(self, text="Search", command=self.start_search).grid(row=0, column=4)
        Button(self, text="Refresh everyting", command=self.restart_program).grid(row=0, column=5, padx=(10, 100))

        graph = SentimentGraph()



        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(graph.figure, master=self.master)
        canvas.mpl_connect('pick_event', self.onclick)
        canvas.show()
        canvas.get_tk_widget().pack(side=RIGHT, fill=BOTH, expand=1)

        toolbar = NavigationToolbar2TkAgg(canvas, self.master)
        toolbar.update()
        canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=1)


        graph = TimelineGraph()

        # a tk.DrawingArea
        canvas = FigureCanvasTkAgg(graph.figure, master=self.master)
        canvas.show()
        canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1, pady=1)

        toolbar = NavigationToolbar2TkAgg(canvas, self.master)
        toolbar.update()
        canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=1)


        # insert

    def create_menu_bar(self):
        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="Settings", command=self.client_settings)
        file.add_command(label="Exit", command=self.client_exit)

        # added "file" to our menu
        menu.add_cascade(label="File", menu=file)

    def client_exit(self):
        self.quit()

    def client_settings(self):
        print("settings menu")

    def start_search(self):
        print(self.searchVar.get())
        self.db.setSearchQuery(str(self.searchVar.get()))
        #start new view
        StatusView()

    def restart_program(self):
        """Restarts the current program.
        Note: this function does not return. Any cleanup action (like
        saving data) must be done before calling this function."""
        python = sys.executable
        os.execl(python, python, * sys.argv)

    def onclick(self, event):
        ind = event.ind
        #get first index of the array
        ind = ind[0]

        #get x and y
        thisline = event.artist
        xdata = thisline.get_xdata() #timestamp
        ydata = thisline.get_ydata() #sentiment

        #cast to usefull stuff
        score = int(ydata[ind])
        timestamp = str(int(matplotlib.dates.num2epoch(xdata[ind])))
        timestamp = timestamp[:-1]

        #find it in the database
        db = Database()
        #print(" select * from tweets WHERE posted_at LIKE '"+ str(timestamp) +"%' AND sentiment_score = '"+ str(score) + "'" )
        result = db.query(" select * from tweets WHERE posted_at LIKE '"+ str(timestamp) +"%' AND sentiment_score = '"+ str(score) + "'")
        row = result.fetchone()

        #order data
        id = row[0]
        name = row[1]
        screenname = row[2]
        posted_at = row[3]
        tweet = row[4]
        location = row[7]
        score = row[10]

        #build the string
        strbuilder = []

        strbuilder.append("id : "+str(id))
        strbuilder.append("\nname : "+name.decode('ascii', 'ignore'))
        strbuilder.append("\nlocation : "+location)
        strbuilder.append("\nscore : "+str(score))
        strbuilder.append("\ntweet : "+tweet.decode('ascii', 'ignore'))
        #show me the good stuff
        messagebox.showinfo("info", ''.join(strbuilder))




