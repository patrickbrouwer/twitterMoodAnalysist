import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.dates as md
import tkinter as Tk
from Core.Database import Database
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
from numpy import arange

matplotlib.use('TkAgg')


class TimelineGraph:
    figure = None

    timeStamps = []
    tweets = []

    def __init__(self, create_figure=True):
        # get data from database.
        self.get_data()

        # create figure.
        if create_figure:
            self.figure = Figure(figsize=(5, 4), dpi=100)

        # draw graph in figure
        self.draw_graph(self.figure)

    def draw_graph(self, figure):
        # add graph to figure.
        a = self.figure.add_subplot(111)

        # set titles.
        a.set_title("Tweets per uur")
        a.set_xlabel("tijd")
        a.set_ylabel("aantal tweets")

        # generate graph.
        a.plot(self.timeStamps, self.tweets)

        xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')

        a.xaxis.set_minor_locator(HourLocator(arange(0, 25, 6)))
        a.xaxis.set_major_formatter(DateFormatter('%H:%M:%S'))
        a.xaxis.set_major_formatter(xfmt)

        a.fmt_xdata = md.DateFormatter('%Y-%m-%d %H:%M:%S')
        self.figure.autofmt_xdate()

    def get_data(self):
        db = Database()
        result = db.query(
            ''' select count() as tweets,posted_at / 1000 as timestamp from tweets group by strftime('%Y-%m-%d %H:%M', posted_at / 1000, 'unixepoch') ''')
        rows = result.fetchall()

        for row in rows:
            self.tweets.append(row[0])
            # convert unix timestamp to matplotlib compatible
            date = matplotlib.dates.epoch2num(row[1])
            self.timeStamps.append(date)
        return True


if __name__ == "__main__":
    # change config db file location
    import config

    config.DB_FILE = "../tweets.db"

    # create window
    root = Tk.Tk()
    root.wm_title("time line")

    graph = TimelineGraph()


    def _quit():
        root.quit()  # stops mainloop
        root.destroy()  # this is necessary on Windows to prevent
        # Fatal Python Error: PyEval_RestoreThread: NULL tstate


    # a tk.DrawingArea
    canvas = FigureCanvasTkAgg(graph.figure, master=root)
    canvas.show()
    canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

    toolbar = NavigationToolbar2TkAgg(canvas, root)
    toolbar.update()
    canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

    button = Tk.Button(master=root, text='Quit', command=_quit)
    button.pack(side=Tk.BOTTOM)

    Tk.mainloop()
    # If you put root.destroy() here, it will cause an error if
    # the window is closed with the window manager.
