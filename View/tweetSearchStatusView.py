from tkinter import *
import os
import config
from Core.Database import Database

class StatusView:

    tweetCount = 0

    def __init__(self):
        root = self.root = Toplevel()
        root.geometry("400x400")
        Label(root, text="Tweet scraper status window").pack()
        Button(root, text='Start live streamer', command=self.streamer_search).pack()
        Button(root, text='Start history search', command=self.history_search).pack()

        self.tweetCount = StringVar()
        self.tweetCount.set("tweet count: loading")
        countLabel = Label(root, textvariable=self.tweetCount).pack()

        self.update_db_counter()

    def history_search(self):
        os.system("start /wait cmd /c python "+config.BASE_DIR+"\startScraper.py search")
        self.update_db_counter()
        return True

    def streamer_search(self):
        os.system("start /wait cmd /c python "+config.BASE_DIR+"\startScraper.py streamer")
        self.update_db_counter()


    def update_db_counter(self):
        db = Database()
        self.tweetCount.set("tweet database count: "+str(db.tweet_count()))

