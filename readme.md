# installed libories
- tweepy (pip install tweepy)
- matplotlib
    - numpy
    - six
    

## install matplotlib
required python 3.4 (not 3.5!)

- install numpy "pip install numpy" (easy way by installer for windows) [http://sourceforge.net/projects/numpy/files/NumPy/1.10.1/]
- run command "pip install matplotlib"
- ready to use!

# other
AFINN - [http://www2.imm.dtu.dk/pubdb/views/publication_details.php?id=6010]


select count() as tweets,strftime('%Y-%m-%d %H:%M', posted_at / 1000, 'unixepoch') as timestamp from tweets group by strftime('%Y-%m-%d %H:%M', posted_at / 1000, 'unixepoch')