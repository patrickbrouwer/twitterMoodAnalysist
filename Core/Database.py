import config, sqlite3


class Database():
    conn = ""
    table = ""
    c = "";

    def __init__(self):
        self.conn = sqlite3.connect(config.DB_FILE)
        self.c = self.conn.cursor()

    # Setup database table if not exist.
    def setup_database(self):
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS tweets
                (ID INTEGER PRIMARY KEY  AUTOINCREMENT,name TEXT, screenname TEXT, posted_at INTEGER, tweet TEXT, retweeted INTEGER, favourited INTEGER, location TEXT,lang TEXT, time_zone TEXT, sentiment_score INTEGER)''')

        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS settings
                (preference TEXT, value TEXT)''')

        if (self.getSearchQuery() == None):
            self.c.execute(
                ''' INSERT INTO settings VALUES("search","koffie") ''')
            self.conn.commit()

        return True

    def all(self, colums="*"):
        return self.conn.execute('SELECT * FROM ' + self.table)

    def insert_tweets(self, data):
        self.conn.execute(
            'INSERT INTO tweets (name, screenname, posted_at, tweet, retweeted, favourited, location, lang, time_zone, sentiment_score) VALUES( ?,?,?,?,?,?,?,?,?,? )',
            data)
        self.conn.commit()

    def check_table_selection(self):
        if (self.table == ""):
            raise Exception('No database table selected')

    def getSearchQuery(self):
        query = self.c.execute(
            ''' SELECT `value` FROM settings WHERE preference = 'search' ''')

        result = query.fetchone()
        if (result == None):
            return None
        return result[0]

    def setSearchQuery(self, searchTerm):

        insert = [
            searchTerm
        ]

        self.c.execute(
            ''' UPDATE settings SET `value` = ? WHERE preference = 'search' ''', insert)
        self.conn.commit()
        return True

    def query(self, query):
        return self.conn.execute(query)

    def tweet_count(self):
        query = self.c.execute(
            ''' SELECT count(`id`) FROM tweets ''')

        result = query.fetchone()
        if result == None:
            return 0
        return result[0]
