import json

import tweepy

from Core import SentimentAnalysis
from Core.Database import Database


# This is the listener, resposible for receiving data
class StdOutListener(tweepy.StreamListener):
    db = Database()

    counter = 0

    def on_data(self, data):
        #print(data)
        # Twitter returns data in JSON format - we need to decode it first
        decoded = json.loads(data)

        try:
            insertTweet = [
                str(decoded['user']['name']).encode('ascii', 'ignore'),
                str(decoded['user']['screen_name']).encode('ascii', 'ignore'),
                str(decoded['timestamp_ms']),
                str(decoded['text']).encode('ascii', 'ignore'),
                decoded['retweeted'],
                decoded['favorited'],
                str(decoded['user']['location']),
                str(decoded['lang']),
                str(decoded['user']['time_zone']),
                SentimentAnalysis.Sentiment_analysis(str(decoded['text']), lang=str(decoded['lang']))
            ]
            self.db.insert_tweets(insertTweet)
        except:
            print("Oops!  That was not valid  Try again...")

        # Also, we convert UTF-8 to ASCII ignoring all bad characters sent by users
        #print('@%s: %s' % (decoded['user']['screen_name'], decoded['text'].encode('ascii', 'ignore')))
        self.counter+=1
        print("tweet Count "+str(self.counter))
        # print('')
        return True

    def on_error(self, status):
        print(status)


def start_streamLister(auth, seachTerm):
    print("start streamer")

    # There are different kinds of streams: public stream, user stream, multi-user streams
    # In this example follow #programming tag
    # For more details refer to https://dev.twitter.com/docs/streaming-apis
    l = StdOutListener()
    stream = tweepy.Stream(auth, l)

    # https://dev.twitter.com/streaming/reference/post/statuses/filter
    stream.filter(track=[seachTerm])

# https://github.com/tweepy/tweepy/tree/master/examples
# https://dev.twitter.com/overview/api/response-codes
