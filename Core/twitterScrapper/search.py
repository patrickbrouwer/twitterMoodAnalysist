import time

from tweepy import API, Cursor

from Core import SentimentAnalysis
from Core.Database import Database


def start_tweet_search(auth, searchTerm, count=10000):
    print("start searching...")
    api = API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    db = Database()
    tweetCount = 0

    for tweet in Cursor(api.search, rpp=100, show_user=False, q=searchTerm, include_entities=True).items(count):
        tweetCount += 1
        insertTweet = None

        # print("Name:", tweet.author.name.encode('utf8'))
        # print("Screen-name:", tweet.author.screen_name.encode('utf8'))
        # print("Tweet created:", tweet.created_at)
        # print("Tweet:", tweet.text.encode('utf8'))
        # print("Retweeted:", tweet.retweeted)
        # print("Favourited:", tweet.favorited)
        # print("Location:", tweet.user.location.encode('utf8'))
        # print("Time-zone:", tweet.user.time_zone)
        # print("Geo:", tweet.geo)
        # print("//////////////////")

        if (tweet.user.time_zone == None):
            tweet.user.time_zone = ""

        insertTweet = [
            str(tweet.author.name).encode('ascii', 'ignore'),
            str(tweet.author.screen_name).encode('ascii', 'ignore'),
            time.mktime(tweet.created_at.timetuple()) * 1000,
            str(tweet.text).encode('ascii', 'ignore'),
            tweet.retweeted,
            tweet.favorited,
            str(tweet.user.location),
            str(tweet.lang),
            str(tweet.user.time_zone),
            SentimentAnalysis.Sentiment_analysis(str(tweet.text), lang=str(tweet.lang))
        ]
        db.insert_tweets(insertTweet)

        if (tweetCount % 100 == 0):
            print("Tweet count " + str(tweetCount))

    print("finished")
