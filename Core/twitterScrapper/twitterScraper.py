import Core.twitterScrapper.search as searcher
import Core.twitterScrapper.streamer as streamer
from Core.Database import Database
from config import *
import threading
from tweepy import auth, OAuthHandler


class twitterScraper:
    history_search_thread = None
    stream_search_thread = None

    stopCommand = False

    def __init__(self):
        db = Database()
        db.setup_database()
        self.searchTerm = db.getSearchQuery()

    def authenicate(self):
        auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        return auth

    def tweet_history_search(self, tweet_count=10000):
        searcher.start_tweet_search(self.authenicate(), self.searchTerm, tweet_count)
        return True

    def tweet_stream_search(self):
        streamer.start_streamLister(self.authenicate(), self.searchTerm)
        return True
