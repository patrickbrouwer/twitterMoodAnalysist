import re

def Sentiment_analysis(sentence, lang='en', base_patch="wordlist/"):
    # split words to array
    sentence_list = sentence.split(' ')
    #print(sentence_list)
    mood_score = 0
    word_list = select_wordlist(lang)

    with open(base_patch + word_list, 'r') as f:
        for line in f:
            word_score = re.split(r'\t+', line) #http://stackoverflow.com/questions/17038426/splitting-a-string-based-on-tab-in-the-file
            #check if word is in list
            if word_score[0] in sentence_list:
                #print("woord found", word_score[0])
                #print("integer", word_score[1])
                mood_score += int(word_score[1])
    return mood_score


def select_wordlist(lang, base_file="AFINN-111.txt"):
    base_file = base_file.split(".")
    base_file_name = base_file[0]
    base_extension = base_file[1]

    if lang == "nl":
        return base_file_name +".nl"+ "." + base_extension
    else:
       return base_file_name + "." + base_extension


#test = Sentiment_analysis("In recent years abandoned abandoned car museums worthless have evolved from a manufacturer's afterthought to must-see destinations in their own right")
#print(test)
