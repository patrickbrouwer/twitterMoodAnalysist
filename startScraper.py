from Core.twitterScrapper.twitterScraper import twitterScraper
import sys

if sys.argv[1] == "streamer":
    stream_scraper = twitterScraper()
    stream_scraper.tweet_stream_search()

if sys.argv[1] == "search":
    search_scraper = twitterScraper()
    search_scraper.tweet_history_search()
