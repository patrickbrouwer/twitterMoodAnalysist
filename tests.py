import unittest
from Core.Database import Database
import os
from Core.SentimentAnalysis import *


class TestStringMethods(unittest.TestCase):

    def test_db_connection(self):
        DB = Database()
        DB.setup_database()
        self.assertTrue(os.path.isfile("tweets.db"))

    def test_db_query_search(self):
        DB = Database()
        self.assertTrue(DB.getSearchQuery() != None)

    def test_sentiment_natural(self):
        self.assertTrue(Sentiment_analysis("dit is een neutrale zin in het Nederlands", "NL") == 0)


    def test_sentiment_negative(self):
        self.assertTrue(Sentiment_analysis("Dit is een erg irritante negative zin", "NL") < 0)

    def test_sentiment_positive(self):
        self.assertTrue(Sentiment_analysis("Dit is een erg leuke positive zin zin", "NL") > 0)

if __name__ == '__main__':
    unittest.main()
